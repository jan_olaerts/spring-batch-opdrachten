package be.janolaerts.springbatchopdrachten.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name="BeerOrderItems")
@XmlRootElement
public class BeerOrderItem {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="Id")
    private int id;

    @ManyToOne
    @JoinColumn(name="BeerId")
    private Beer beer;

    @Column(name="Number")
    private int amount;

    public BeerOrderItem() {
    }

    public BeerOrderItem(Beer beer, int amount) {
        this.beer = beer;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    @XmlTransient
    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int number) {
        this.amount = number;
    }

    @Override
    public String toString() {
        return "BeerOrderItem{" +
                "id=" + id +
                ", beer=" + beer +
                ", number=" + amount +
                '}';
    }
}