package be.janolaerts.springbatchopdrachten.reader;

import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.stereotype.Component;

import java.util.function.DoubleToIntFunction;

@Component
public class NumberReader implements ItemReader<Integer>, ItemStream {

    private Integer[] numbers = new Integer[] {5, 6, 4, 8, 2};
    private int index = -1;

    private static final String INDEX = "NumberReader.index";

    @Override
    public synchronized Integer read() {

        Integer num = null;

        if(++index < numbers.length)
            num = numbers[index];

        System.out.println("Read: " + num);
        return num;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        System.out.println("Open");
        index = executionContext.getInt(INDEX, -1);
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
        System.out.println("Update");
        executionContext.putInt(INDEX, index);
    }

    @Override
    public void close() throws ItemStreamException {
        System.out.println("Close");
    }

    @BeforeStep
    public void beforeStep() {
        System.out.println("Before Step");
    }

    @AfterStep
    public void afterStep() {
        System.out.println("After Step");
    }
}