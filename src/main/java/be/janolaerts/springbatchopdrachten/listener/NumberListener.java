package be.janolaerts.springbatchopdrachten.listener;

import org.springframework.stereotype.Component;

import javax.batch.api.chunk.listener.ChunkListener;

// Todo: ask Ward: three methods are never called

@Component
public class NumberListener implements ChunkListener {

    @Override
    public void beforeChunk() {
        System.out.println("Before chunk execution");
    }

    @Override
    public void onError(Exception e) {
        System.out.println("On error execution");
    }

    @Override
    public void afterChunk() {
        System.out.println("After chunk execution");
    }
}