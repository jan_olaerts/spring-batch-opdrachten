package be.janolaerts.springbatchopdrachten.writer;

import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class NumberWriter implements ItemWriter<Integer> {

    @Override
    public void write(List<? extends Integer> numbers) {
        numbers.forEach(System.out::println);
    }
}