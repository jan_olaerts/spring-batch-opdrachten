package be.janolaerts.springbatchopdrachten;

import be.janolaerts.springbatchopdrachten.exception.NumberException;
import be.janolaerts.springbatchopdrachten.listener.NumberListener;
import be.janolaerts.springbatchopdrachten.processor.NumberProcessor;
import be.janolaerts.springbatchopdrachten.reader.NumberReader;
import be.janolaerts.springbatchopdrachten.tasklet.FailedTasklet;
import be.janolaerts.springbatchopdrachten.tasklet.FinishedTasklet;
import be.janolaerts.springbatchopdrachten.writer.NumberWriter;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@SpringBootApplication
@EnableBatchProcessing
public class BatchApplication {

    @Autowired
    private JobBuilderFactory jobBuilder;

    @Autowired
    private StepBuilderFactory stepBuilder;

    @Bean
    public TaskExecutor taskExecute() {
        return new SimpleAsyncTaskExecutor();
    }

    @Bean
    protected JobLauncher jobLaunch(JobRepository jobRepository,
                                      TaskExecutor taskExecute) {

        SimpleJobLauncher launcher = new SimpleJobLauncher();
        launcher.setJobRepository(jobRepository);
        launcher.setTaskExecutor(taskExecute);
        return launcher;
    }

    @Bean
    protected Step numbersStep(NumberReader reader,
                               NumberProcessor processor,
                               NumberWriter writer,
                               TaskExecutor taskExecute,
                               NumberListener listener) {

        return stepBuilder
                .get("numbersStep")
                .<Integer, Integer> chunk(1)
                .faultTolerant()
                .retry(NumberException.class)
                .retryLimit(1)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .taskExecutor(taskExecute)
                .throttleLimit(3)
                .listener(listener)
                .build();
    }

    @Bean
    protected Step failedStep(FailedTasklet tasklet) {
        return stepBuilder
                .get("failedStep")
                .tasklet(tasklet)
                .build();
    }

    @Bean
    protected Step finishedStep(FinishedTasklet tasklet) {
        return stepBuilder
                .get("finishedStep")
                .tasklet(tasklet)
                .build();
    }

    @Bean
    public Job numbersJob(@Qualifier("numbersStep") Step numbersStep,
                          @Qualifier("failedStep") Step failedStep,
                          @Qualifier("finishedStep") Step finishedStep) {

        // Todo: ask Ward: Not sure about the order
        return jobBuilder
                .get("numbersJob")
                .start(numbersStep)
                .on("FAILED")
                .to(failedStep)
                .from(numbersStep)
                .on("*")
                .to(finishedStep)
                .build()
                .build();
    }

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx =
            SpringApplication.run(BatchApplication.class);

        Job job = ctx.getBean("numbersJob", Job.class);
        JobLauncher jobLauncher = ctx.getBean("jobLauncher", JobLauncher.class);

        JobParametersBuilder builder = new JobParametersBuilder();
        builder.addLong("number", 1L);
        builder.addString("comment", "Listener Test");
        JobParameters jobParameters = builder.toJobParameters();

        int trial = 1;
        JobExecution jobExecution;
        do {
            System.out.println("Trial " + trial++);
            jobExecution = jobLauncher.run(job, jobParameters);
        } while(jobExecution.getStatus() == BatchStatus.FAILED);
    }
}