package be.janolaerts.springbatchopdrachten;

import be.janolaerts.springbatchopdrachten.entity.Beer;
import be.janolaerts.springbatchopdrachten.processor.BeerProcessor;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.persistence.EntityManagerFactory;
import java.util.Random;

@SpringBootApplication
@EnableBatchProcessing
public class BeerBatchApplication {

    @Autowired
    private JobBuilderFactory jobBuilder;

    @Autowired
    private StepBuilderFactory stepBuilder;

    @Autowired
    private EntityManagerFactory emf;

    @Bean
    public JpaPagingItemReader<Beer> beerReader() {

        JpaPagingItemReader<Beer> reader =
                new JpaPagingItemReader<>();

        reader.setEntityManagerFactory(emf);
        reader.setQueryString("select b from Beer b where b.stock < 10");
        reader.setPageSize(Integer.MAX_VALUE);
        return reader;
    }

    @Bean
    public ItemWriter<Beer> beerWriter() {

        JpaItemWriter<Beer> writer = new JpaItemWriter<>();
        writer.setEntityManagerFactory(emf);
        return writer;
    }

    @Bean
    public Step backorderStep(ItemReader<Beer> beerReader,
                              BeerProcessor beerProcessor,
                              ItemWriter<Beer> beerWriter) {

        return stepBuilder
                .get("backorderStep")
                .<Beer, Beer>chunk(10)
                .reader(beerReader)
                .processor(beerProcessor)
                .writer(beerWriter)
                .build();
    }

    @Bean
    public Job beerJob(Step backorderStep) {

        return jobBuilder
                .get("beerJob")
                .start(backorderStep)
                .build();
    }

    public static void main(String[] args) throws Exception {

        ApplicationContext ctx = SpringApplication.run(
                BeerBatchApplication.class);

        Random rand = new Random();
        Job job = ctx.getBean("beerJob", Job.class);
        JobLauncher jobLauncher = ctx.getBean("jobLauncher", JobLauncher.class);

        JobParametersBuilder builder = new JobParametersBuilder();
        builder.addLong("number", rand.nextLong(), true);
        JobParameters jobParameters = builder.toJobParameters();
        jobLauncher.run(job, jobParameters);
    }
}