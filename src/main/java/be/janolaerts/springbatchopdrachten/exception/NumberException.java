package be.janolaerts.springbatchopdrachten.exception;

public class NumberException extends RuntimeException {

    public NumberException(String message) {
        super(message);
    }
}