package be.janolaerts.springbatchopdrachten.processor;

import be.janolaerts.springbatchopdrachten.entity.Beer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class BeerProcessor implements ItemProcessor<Beer, Beer> {

    @Override
    public Beer process(Beer beer) {

        System.out.println("Process beer: " + beer.getId());
        beer.setStock(10);
        return beer;
    }
}