package be.janolaerts.springbatchopdrachten.processor;

import be.janolaerts.springbatchopdrachten.exception.NumberException;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class NumberProcessor implements ItemProcessor<Integer, Integer> {

    private Random rand = new Random();

    @Override
    public Integer process(Integer num) {

        if(rand.nextBoolean())
            throw new NumberException("Random was true");

        return num * num;
    }
}